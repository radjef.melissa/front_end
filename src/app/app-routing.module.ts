import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LivreListComponent} from './components/livre/livre-list/livre-list.component'
import {LivreDetailsComponent} from './components/livre/livre-details/livre-details.component'
import {AddLivreComponent} from './components/livre/add-livre/add-livre.component'
import {HomeComponent} from './components/home/home.component'
import {UtilListComponent} from './components/utilisateur/util-list/util-list.component'
import {HomeUtilComponent} from 'src/app/components/home-util/home-util.component'
import {PanierComponent} from 'src/app/components/panier/panier.component'
import { InscriptionComponent } from './components/inscription/inscription.component';
import { ConnexionComponent } from './components/connexion/connexion.component';
import { ProfileComponent } from './components/profile/profile.component';
import {LogoutComponent} from 'src/app/components/logout/logout.component'

const routes: Routes = [
  {path: '', pathMatch:  "full",redirectTo:  "home"},
  {path: 'admin', pathMatch:  "full",redirectTo:  "admin/home"},
  {path:'admin/home', component: HomeComponent},
  {path: 'admin/livres', component: LivreListComponent},
  {path: 'admin/livres/:id', component: LivreDetailsComponent},
  {path: 'admin/add', component: AddLivreComponent},
  {path: 'livres', component: UtilListComponent},
  {path: 'home', component: HomeUtilComponent},
  {path: 'panier', component: PanierComponent},
  {path: 'register', component:InscriptionComponent},
  {path: 'login', component:ConnexionComponent},
  {path: 'profile', component:ProfileComponent}, 
  {path: 'logout', component:LogoutComponent}, 
  

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
