import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule,  ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule} from  '@angular/material/toolbar';
import { MatIconModule} from  '@angular/material/icon';
import { MatSidenavModule } from  '@angular/material/sidenav';
import { MatListModule } from  '@angular/material/list';
import { MatButtonModule } from  '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AddLivreComponent } from './components/livre/add-livre/add-livre.component';
import { LivreDetailsComponent } from './components/livre/livre-details/livre-details.component';
import { LivreListComponent } from './components/livre/livre-list/livre-list.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { HomeComponent } from './components/home/home.component';

// search module
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { UtilListComponent } from './components/utilisateur/util-list/util-list.component';
import { NavbarUtilComponent } from './components/navbar-util/navbar-util.component';
import { NavbarUtilService } from './services/navbar-util/navbar-util.service';
import { HomeUtilComponent } from './components/home-util/home-util.component';
import { PanierComponent } from './components/panier/panier.component';
import { CommandeComponent } from 'src/app/components/commande/commande.component';


import { InscriptionComponent } from './components/inscription/inscription.component';
import { ProfileComponent } from './components/profile/profile.component';
import { ConnexionComponent } from './components/connexion/connexion.component';
import { UtilisateurService } from './services/utilisateur/utilisateur.service';
import { LogoutComponent } from './components/logout/logout.component';


@NgModule({
  declarations: [
    AppComponent,
    AddLivreComponent,
    LivreDetailsComponent,
    LivreListComponent,
    NavbarComponent,
    HomeComponent,
    UtilListComponent,
    NavbarUtilComponent,
    HomeUtilComponent,
    PanierComponent,
    CommandeComponent,
    InscriptionComponent,
    ProfileComponent,
    ConnexionComponent,
    LogoutComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    ReactiveFormsModule,
    AppRoutingModule,
    Ng2SearchPipeModule
  ],
  providers: [NavbarUtilService,
  UtilisateurService],
  bootstrap: [AppComponent]
})
export class AppModule {
  
 }
