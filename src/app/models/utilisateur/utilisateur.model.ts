export class Utilisateur {
    nom? : string
    prenom? : string
    dateNaissance?: String
    genre?: String
    adresse?: String
    numCarte?: String
    dateCarte?: String
    codeCarte?: String
    email?: String
    password?: String

}
