import { Livre } from 'src/app/models/livre/livre.model'

export class Commande {
    _id?: any
    email?: string
    total?: number
    achats?: Livre[]

}
