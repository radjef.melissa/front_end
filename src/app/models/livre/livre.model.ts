export class Livre {
    _id?: any
    titre? : string
    auteurs? : string
    datePublication? : Date
    genre? : string
    quatriemePage? : string
    prix? : number
    quantite?: number
    qtAchete?: number
}
