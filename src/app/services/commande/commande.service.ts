import { Injectable } from '@angular/core';

import { HttpClient, HttpParams } from '@angular/common/http';
import { from, Observable } from 'rxjs';
import {Commande} from 'src/app/models/commande/commande.model'

const baseUrl = 'http://localhost:8080/achat';

@Injectable({
  providedIn: 'root'
})
export class CommandeService {


  constructor(private http: HttpClient) { }

  getAll(): Observable<Commande[]> {

    return this.http.get<Commande[]>(baseUrl,{
      observe: 'body',
      params: new HttpParams().append('token', localStorage.getItem('token')!)
    
    });
  }

  get(id: any): Observable<Commande> {
    return this.http.get(`${baseUrl}/${id}`,{
      observe: 'body',
      params: new HttpParams().append('token', localStorage.getItem('token')!)
    
    });
  }

  create(data: any): Observable<any> {
    return this.http.post(baseUrl, data,{
      params: new HttpParams().append('token', localStorage.getItem('token')!)
    });
  }

}
