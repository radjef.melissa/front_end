import { TestBed } from '@angular/core/testing';

import { NavbarUtilService } from './navbar-util.service';

describe('NavbarUtilService', () => {
  let service: NavbarUtilService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NavbarUtilService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
