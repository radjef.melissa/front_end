import { Injectable, ɵɵresolveBody } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Utilisateur } from 'src/app/models/utilisateur/utilisateur.model';


const baseUrl = 'http://localhost:8080/user';
const logUrl = 'http://localhost:8080/login';
const emailUrl= "http://localhost:8080/user/test"

@Injectable({
  providedIn: 'root'
})
export class UtilisateurService {

  constructor(private http: HttpClient) { }

  submitRegister(data: any): Observable<any> {
    return this.http.post(baseUrl, data,{
      observe: 'body',
      params: new HttpParams().append('token', localStorage.getItem('token')!)
    
    });
  }

  login(data:any): Observable<any>{
    return this.http.post(logUrl,data,{
      observe: 'body',
      params: new HttpParams().append('token', localStorage.getItem('token')!)
    
    });
  }

  getinfo(){
    return this.http.get(logUrl, {
      observe: 'body',
      // @ts-ignore: Object is possibly 'null'.
      params: new HttpParams().append('token', localStorage.getItem('token'))
    });
  }

  get(email: any): Observable<Utilisateur> {
    return this.http.get(`${emailUrl}/email=${email}`,{
      observe: 'body',
      params: new HttpParams().append('token', localStorage.getItem('token')!)
    
    });
  }

}
