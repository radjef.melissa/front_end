import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { from, Observable } from 'rxjs';
import { Livre } from '../../models/livre/livre.model'

const baseUrl = 'http://localhost:8080/livre';

@Injectable({
  providedIn: 'root'
})
export class LivreService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<Livre[]> {
    return this.http.get<Livre[]>(baseUrl,{
      observe: 'body',
      params: new HttpParams().append('token', localStorage.getItem('token')!)
    
    });
  }

  getAllAdmin(): Observable<Livre[]> {
    console.log("un soucis")
    return this.http.get<Livre[]>(`${baseUrl}/administrateur`,{
      observe: 'body',
      params: new HttpParams().append('token', localStorage.getItem('token')!)
    
    });
  }

  get(id: any): Observable<Livre> {
    return this.http.get(`${baseUrl}/${id}`,{
      observe: 'body',
      params: new HttpParams().append('token', localStorage.getItem('token')!)
    
    });
  }

  create(data: any): Observable<any> {
    return this.http.post(`${baseUrl}/administrateur`, data,{
      observe: 'body',
      params: new HttpParams().append('token', localStorage.getItem('token')!)
    
    });
  }

  update(id: any, data: any): Observable<any> {
    return this.http.put(`${baseUrl}/${id}`, data,{
      observe: 'body',
      params: new HttpParams().append('token', localStorage.getItem('token')!)
    
    });
  }

  delete(id: any): Observable<any> {
    return this.http.delete(`${baseUrl}/${id}`,{
      observe: 'body',
      params: new HttpParams().append('token', localStorage.getItem('token')!)
    
    });
  }

  deleteAll(): Observable<any> {
    return this.http.delete(baseUrl,{
      observe: 'body',
      params: new HttpParams().append('token', localStorage.getItem('token')!)
    
    });
  }

  findByTitle(title: any): Observable<Livre[]> {
    return this.http.get<Livre[]>(`${baseUrl}?titre=${title}`,{
      observe: 'body',
      params: new HttpParams().append('token', localStorage.getItem('token')!)
    
    });
  }


}
