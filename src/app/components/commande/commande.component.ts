import { Component, OnInit } from '@angular/core';
import {Commande} from 'src/app/models/commande/commande.model'
import {CommandeService} from 'src/app/services/commande/commande.service'

@Component({
  selector: 'app-commande',
  templateUrl: './commande.component.html',
  styleUrls: ['./commande.component.css']
})
export class CommandeComponent implements OnInit {

  commande: Commande = {
  email: '',
  total: 0,
  achats: [],
}

  constructor(private commandeService: CommandeService) { }

  ngOnInit(): void { 
    
  } 

  public saveCommande(total: any):void {


    const data = {
      
      email: localStorage.getItem("email"),
      total: total,
      achats: JSON.parse(localStorage.getItem('listCommande')|| '[]')
    };
    

    console.log(data)
    

    this.commandeService.create(data)
      .subscribe(
        response => {
          console.log(response);
        },
        error => {
          console.log(error)
        }
      );
  }

  validCommande(): number | void{
    var livres = JSON.parse(localStorage.getItem('listCommande')|| '[]');
    var i;
    for(var livre in livres){
      if(livres[livre].qtAchete > livres[livre].quantite){
        i = Number(livre);
        break;
      }else{
        i = -1;
      }
    }
    return i;
      }

  

  

  // newLivre(): void {
  //   this.commande = {
  //     idUtilisateur: '',
  //     total: 0,
  //     adresse: '',
  //     achats: []
  //   };
  //   this.achats.push(livre);
  // }

}
