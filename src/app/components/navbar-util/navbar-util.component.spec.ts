import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NavbarUtilComponent } from './navbar-util.component';

describe('NavbarUtilComponent', () => {
  let component: NavbarUtilComponent;
  let fixture: ComponentFixture<NavbarUtilComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NavbarUtilComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NavbarUtilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
