import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UtilisateurService } from 'src/app/services/utilisateur/utilisateur.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  a = ''
  constructor(private _user:UtilisateurService,
              private _router:Router,
              private _activatedrouter: ActivatedRoute) { 
    this._user.getinfo()
      .subscribe(
      data => this.a = data.toString(),
      error => this._router.navigate(['/login'])
    )
  }

  ngOnInit(): void {
  }



}
