import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Utilisateur } from 'src/app/models/utilisateur/utilisateur.model';
import { UtilisateurService } from 'src/app/services/utilisateur/utilisateur.service';

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.css']
})
export class InscriptionComponent implements OnInit {

  test?: Utilisateur;
  erreurEmail: boolean = false;

  form: FormGroup;
  successMessage = '';
  constructor(private _user: UtilisateurService,
    private _router: Router,
    private _activatedRouter: ActivatedRoute) {
    this.form = new FormGroup({
      nom: new FormControl(null, Validators.required),
      prenom: new FormControl(null, Validators.required),
      dateNaissance: new FormControl(null),
      genre: new FormControl(null),
      adresse: new FormControl(null, Validators.required),
      numCarte: new FormControl(null, Validators.required),
      dateCarte: new FormControl(null, Validators.required),
      codeCarte: new FormControl(null, Validators.required),
      email: new FormControl(null, Validators.email),
      password: new FormControl(null, Validators.required),
      cnfpass: new FormControl(null, this.passValidator)

    });

    this.form.controls.password.valueChanges.subscribe(
      _x => this.form.controls.cnfpass.updateValueAndValidity()
    );

  }

  ngOnInit(): void {
  }

  isValid(controlName: string) {
    // @ts-ignore: Object is possibly 'null'.
    return this.form.get(controlName).invalid && this.form.get(controlName).touched;
  }

  passValidator(control: AbstractControl) {
    if (control && (control.value !== null || control.value !== undefined)) {
      const cnfpassValue = control.value;

      const passControl = control.root.get('password');
      if (passControl) {
        const passValue = passControl.value;
        if (passValue !== cnfpassValue || passValue === '') {
          return {
            isError: true
          };
        }
      }
    }

    return null;
  }


  register() {
    if (this.form.valid) {


      // if(this.testEmail()){
      console.log(this.form.value);
      this.successMessage = this.form.value;
      this._user.submitRegister(this.form.value)
        .subscribe(
          (_data: any) => this.successMessage = 'Correctement inscrit',
          (_error: any) => this.successMessage = JSON.stringify(_error)
        );
      // }
      // else{
      //   this.erreurEmail = true;
      // }
    }
  }

  moveToLogin() {

    this._router.navigate(['/login'], { relativeTo: this._activatedRouter });

  }

  // testEmail(){
  //   console.log("hey voici le form",this.form.value.email )
  //   console.log(this._user.get(this.form.value.email).subscribe(
  //     data => {
  //       this.test = data;
  //     }));
  //   console.log("dans le test",this.test)

  //   this._user.get(this.form.value.email).subscribe(
  //     data => {
  //       this.test = data;
  //     },
  //     error => {
  //       console.log(error);
      
  //     });
  //     console.log("heys voici ce que je trouve en bdd",this.test)

  //     if (this.test != null){
  //       return this.erreurEmail=true;

  //     }else{
  //       return this.erreurEmail=false;
  //     }
  // }

}
