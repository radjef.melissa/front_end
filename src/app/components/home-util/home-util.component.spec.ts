import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeUtilComponent } from './home-util.component';

describe('HomeUtilComponent', () => {
  let component: HomeUtilComponent;
  let fixture: ComponentFixture<HomeUtilComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeUtilComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeUtilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
