import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  constructor(private _router:Router) { }

  ngOnInit(): void {
    this.logout()
    
  }

  logout(){
    localStorage.removeItem('token');
    localStorage.removeItem('email');
    this._router.navigate(['/login'])
  }



}
