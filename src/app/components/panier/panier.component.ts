import { AbstractType, Component, OnInit } from '@angular/core';
import { Livre } from 'src/app/models/livre/livre.model';
import {CommandeComponent} from 'src/app/components/commande/commande.component';
import { Commande } from 'src/app/models/commande/commande.model';
import {LivreService} from 'src/app/services/livre/livre.service' 

import { ActivatedRoute, Router } from '@angular/router';

@Component({
  providers:[CommandeComponent],
  selector: 'app-panier',
  templateUrl: './panier.component.html',
  styleUrls: ['./panier.component.css']
})
export class PanierComponent implements OnInit {



  items: Livre[] = [];

  livres?: Livre[];
  currentLivre?: Livre;
  currentIndex= -1;
  titre = '';
  total: number = 0;
  index: number= -1;
  message = '';
  livreProb?: Livre;

  

  constructor(private commandeComponent: CommandeComponent, 
    private livreService: LivreService,    
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    this.retrievePanier()
    this.calculerTotal(this.livres)
  }

  retrievePanier(): void{

    if ("token" in localStorage){
      this.livres = JSON.parse(localStorage.getItem('listCommande')|| '[]');
      

    }
    else{
      this.router.navigate(['/login']);
  }
}


  refreshList(): void {
    this.retrievePanier();
    this.currentLivre = undefined;
    this.currentIndex = -1;
  }

  setActiveLivre(livre: Livre, index: number): void {
    this.currentLivre = livre;
    this.currentIndex = index;
  }

  clearPanier() {
    this.items = [];
    localStorage.setItem('listCommande', JSON.stringify(this.items));
    window.location.reload();}

  validerPanier(total: any) {
    if (this.commandeComponent.validCommande() == -1){
      this.commandeComponent.saveCommande(total);

      for(var livre in this.livres){
        this.livres[Number(livre)].quantite = this.livres[Number(livre)].quantite! - this.livres[Number(livre)].qtAchete!; 
        
        this.updateQtLivre(this.livres[Number(livre)]);
        this.deleteLivre(this.livres[Number(livre)]);
      }

      this.clearPanier();
    }else{
      this.index = this.commandeComponent.validCommande() || 0;
      this.livreProb = this.livres![Number(this.index)]
    }
    

  }

  calculerTotal(livres: any){
    this.total = 0;
    for(var livre in livres){
      this.total = (livres[livre].prix * livres[livre].qtAchete)  + this.total;
    }
    
    
  }

  majQuantiteAchete(): void{
    this.calculerTotal(this.livres)
    localStorage.setItem('listCommande', JSON.stringify(this.livres));
  }

  updateQtLivre(livre: any): void {
    this.livreService.update(livre._id, livre)
      .subscribe(
        response => {
          console.log(response);
          this.message = response.message;
        },
        error => {
          console.log(error);
        });
  }

  deleteLivre(livre: any): void{

    if(livre.quantite == 0){

      this.livreService.delete(livre._id)
      .subscribe(
        response => {
          console.log(response);
          this.router.navigate(['/livres']);
        },
        error => {
          console.log(error);
        });

    }
  }

  deleteFromPanier(livre: any): void{

    this.items = this.livres!.filter(item => item._id != livre._id);
    localStorage.setItem('listCommande', JSON.stringify(this.items));
    window.location.reload();

  }


}
