import { Component, OnInit } from '@angular/core';
import { LivreService } from 'src/app/services/livre/livre.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Livre } from 'src/app/models/livre/livre.model';

@Component({
  selector: 'app-livre-details',
  templateUrl: './livre-details.component.html',
  styleUrls: ['./livre-details.component.css']
})
export class LivreDetailsComponent implements OnInit {
  currentLivre: Livre = {
    titre: '',
    auteurs:'',
    datePublication: undefined,
    genre: '',
    prix: undefined,
    quantite: undefined
  };
  message = '';

  constructor(
    private livreService: LivreService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    this.message = '';
    this.getLivre(this.route.snapshot.params.id);
  }

  getLivre(id: string): void {
    this.livreService.get(id)
      .subscribe(
        data => { 
          this.currentLivre = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  updateLivre(): void {
    this.livreService.update(this.currentLivre._id, this.currentLivre)
      .subscribe(
        response => {
          console.log(response);
          this.message = response.message;
        },
        error => {
          console.log(error);
        });
  }

  deleteLivre(): void {
    this.livreService.delete(this.currentLivre._id)
      .subscribe(
        response => {
          console.log(response);
          this.router.navigate(['/admin/livres']);
        },
        error => {
          console.log(error);
        });
  }



}
