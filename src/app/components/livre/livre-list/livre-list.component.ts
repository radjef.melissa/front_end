import { Component, OnInit } from '@angular/core';
import { Livre } from 'src/app/models/livre/livre.model';
import { LivreService } from 'src/app/services/livre/livre.service';

import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-livre-list',
  templateUrl: './livre-list.component.html',
  styleUrls: ['./livre-list.component.css']
})
export class LivreListComponent implements OnInit {

  livres?: Livre[];
  currentLivre?: Livre;
  currentIndex= -1;
  titre = '';
  constructor(private livreService: LivreService,
    private _router:Router) { }

  ngOnInit(): void {
    this.retrieveLivres();
  }
  retrieveLivres(): void{
    
    
    this.livreService.getAllAdmin()
      .subscribe(
        data => {
          this.livres = data;
          console.log(data);
        },
        error => {
          this._router.navigate(['/login'])
          console.log(error);
        
        });
  }

  refreshList(): void {
    this.retrieveLivres();
    this.currentLivre = undefined;
    this.currentIndex = -1;
  }

  setActiveLivre(livre: Livre, index: number): void {
    this.currentLivre = livre;
    this.currentIndex = index;
  }

  removeAllLivres(): void {
    this.livreService.deleteAll()
      .subscribe(
        response => {
          console.log(response);
          this.refreshList();
        },
        error => {
          console.log(error);
        }
      );

  }
  searchTitle(): void {
    this.livreService.findByTitle(this.titre)
      .subscribe(
        data => {
          this.livres = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }


  
}
