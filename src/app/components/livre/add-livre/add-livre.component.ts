import { Component, OnInit } from '@angular/core';

import { Livre } from 'src/app/models/livre/livre.model'
import {LivreService} from 'src/app/services/livre/livre.service' 
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-add-livre',
  templateUrl: './add-livre.component.html',
  styleUrls: ['./add-livre.component.css']
})
export class AddLivreComponent implements OnInit {

  livre: Livre = {
    titre: '',
    auteurs: '',
    datePublication:  new Date(),
    genre: '',
    quatriemePage: '',
    prix: 0,
    quantite: 0
  };
  submitted = false;
  droit = false;

  constructor(private livreService: LivreService,
    private _router:Router) { }

  ngOnInit(): void {
    this.verifyIdentity();
  }

  saveLivre():void {
    const data = {
      titre: this.livre.titre,
      auteurs: this.livre.auteurs,
      datePublication: this.livre.datePublication,
      genre: this.livre.genre,
      quatriemePage: this.livre.quatriemePage,
      prix: this.livre.prix,
      quantite: this.livre.quantite
    };
    

    this.livreService.create(data)
      .subscribe(
        response => {
          console.log(response);
          this.submitted = true;
        },
        error => {
          console.log(error)
        }
      );

  }
  newLivre(): void {
    this.submitted = false;
    this.livre = {
      titre: '',
      auteurs: '',
      datePublication:  new Date(),
      genre: '',
      quatriemePage: '',
      prix: 0,
      quantite: 0
    };
  }

  verifyIdentity(): void {
    if ("token" in localStorage && localStorage["token"]=="admin_token") {
      this.droit = true;
  } else {
      this.droit = false;
      this._router.navigate(['/login'])
  }
  
  }



}
