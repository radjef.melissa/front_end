import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormGroup, FormControl, Validators } from '@angular/forms';

import { UtilisateurService } from 'src/app/services/utilisateur/utilisateur.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.css']
})
export class ConnexionComponent implements OnInit {

  loginForm: FormGroup;
  constructor(private _user: UtilisateurService,
    private _router: Router,
    private _activatedRouter: ActivatedRoute) {
    this.loginForm = new FormGroup({

      email: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required)

    });

  }

  ngOnInit(): void {
  }

  isValid(controlName: string) {
    // @ts-ignore: Object is possibly 'null'.
    return this.loginForm.get(controlName).invalid && this.loginForm.get(controlName).touched;
  }

  login() {

    console.log(this.loginForm.value);
    if (this.loginForm.valid) {
      this._user.login(this.loginForm.value)
        .subscribe(
          data => {
            console.log(data);
            if(this.loginForm.value.email == "admin@admin.com" && this.loginForm.value.password == "admin" ){
            localStorage.setItem('token', "admin_token");
            this._router.navigate(['/admin/home']);
            }
            else{
            localStorage.setItem('token', data.toString());
            localStorage.setItem('email', this.loginForm.value.email);
            this._router.navigate(['/home']);
            }
            
          },
          error => { }
        );
    }
  }

  moveToRegister() {

    this._router.navigate(['/register'], { relativeTo: this._activatedRouter });

  }

}
