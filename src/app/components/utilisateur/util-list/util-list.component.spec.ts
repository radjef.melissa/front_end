import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UtilListComponent } from './util-list.component';

describe('UtilListComponent', () => {
  let component: UtilListComponent;
  let fixture: ComponentFixture<UtilListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UtilListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UtilListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
