import { Component, OnInit } from '@angular/core';
import {NavbarUtilService} from 'src/app/services/navbar-util/navbar-util.service'
import { LivreService } from 'src/app/services/livre/livre.service';
import { Livre } from 'src/app/models/livre/livre.model';

import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-util-list',
  templateUrl: './util-list.component.html',
  styleUrls: ['./util-list.component.css'] 
})
export class UtilListComponent implements OnInit {

  livres?: Livre[];
  items: Livre[] = [];
  currentLivre?: Livre;
  currentIndex= -1;
  titre = '';
  email = '';
  submitted = false;
  qtAchete: number = 0;

  constructor(private livreService: LivreService,
    private route: ActivatedRoute,
    private _router:Router,) {}

  ngOnInit(): void {
    this.retrieveLivres();
  }
  retrieveLivres(): void{
    this.livreService.getAll()
      .subscribe(
        data => {
          this.livres = data;
          console.log(data); 
        },
        error => {
          this._router.navigate(['/login']);
        
        });
  }

  refreshList(): void {
    this.retrieveLivres();
    this.currentLivre = undefined;
    this.currentIndex = -1;
  }

  setActiveLivre(livre: Livre, index: number): void {
    this.submitted = false;
    this.currentLivre = livre;
    this.currentIndex = index;
  }


  searchTitle(): void {
    this.livreService.findByTitle(this.titre)
      .subscribe(
        data => {
          this.livres = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }



  addToCart(livre: Livre){
    if (JSON.parse(localStorage.getItem('listCommande')!) === null){
      this.items.push(livre);
      localStorage.setItem('listCommande', JSON.stringify(this.items));
    
    }else{

      this.items = JSON.parse(localStorage.getItem('listCommande')!);
      console.log(this.items.find(item => item._id === livre._id))
      console.log(this.items.findIndex(item => item._id === livre._id));

      if(this.items.some(item => item._id === livre._id)){
        console.log("Livre est déjà dans le panier");
        var i = this.items.findIndex(item => item._id === livre._id);
        this.items[i].qtAchete = this.items[i].qtAchete! + 1;
        localStorage.setItem('listCommande', JSON.stringify(this.items));

        // this.items[i].quantite = this.items[i].quantite! + 1;
        console.log(this.items[i].qtAchete);

      }else{
        livre.qtAchete = 1;
        this.items.push(livre);
        localStorage.setItem('listCommande', JSON.stringify(this.items));

      }



      // for(var i in this.items){
      //   // console.log("le livre actuel",livre);
      //   // console.log("la liste des items",this.items[i]);
      //   if(livre._id == this.items[i]._id) {
      //     console.log("Livre est déjà dans le panier");
      //     this.items[i].quantite = this.items[i].quantite! + 1;
      //     console.log(this.items[i].quantite);
      //     // this.submitted = true; 
      //   }else {
      //     this.items.push(livre);
      //     localStorage.setItem('listCommande', JSON.stringify(this.items));
      //   }
        
      //   // this.items.push(livre);
      //   // localStorage.setItem('listCommande', JSON.stringify(this.items));}
      // }
      this.submitted = true;   
    }
  }
  
}